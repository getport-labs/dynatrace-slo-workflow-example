import requests
import json
from decouple import config

PORT_API_URL = "https://api.getport.io/v1"
ENVIRONEMNT_ID = config("DYNATRACE_ENVIRONMENT_ID")
API_TOKEN= config("DYNATRACE_API_KEY")
CLIENT_ID = config("PORT_CLIENT_ID")
CLIENT_SECRET = config("PORT_CLIENT_SECRET")
BLUEPRINT_ID='microservice'
## Get Access Token
credentials = {'clientId': CLIENT_ID, 'clientSecret': CLIENT_SECRET}
token_response = requests.post(f'{PORT_API_URL}/auth/access_token', json=credentials)
access_token = token_response.json()['accessToken']
port_headers = {
	'Authorization': f'Bearer {access_token}'
}

def add_entity_to_port(entity_object):
    """A function to create the passed entity in Port

    Params
    --------------
    entity_object: dict
        The entity to add in your Port catalog
    
    Returns
    --------------
    response: dict
        The response object after calling the webhook
    """
    response = requests.post(f'{PORT_API_URL}/blueprints/{BLUEPRINT_ID}/entities?upsert=true&merge=true', json=entity_object, headers=port_headers)
    print(response.json())



# Set up authentication
headers = {
    'Authorization': f'Api-Token {API_TOKEN}',
    'Content-Type': 'application/json'
}

# Set up pagination
pageSize = 1000
nextPageKey = None
slos = []

# Retrieve all SLOs with pagination applied
while True:
    # Construct API request URL with pagination and timeframe
    url = f'https://{ENVIRONEMNT_ID}.live.dynatrace.com/api/v2/slo?pageSize={pageSize}'
    if nextPageKey:
        url += f'&nextPageKey={nextPageKey}'

    # Make API request to retrieve SLOs
    response = requests.get(url, headers=headers)
    
    # Extract the SLOs and paging information from the response
    data = json.loads(response.text)

    slos += data['slo']
    nextPageKey = data.get('nextPageKey')
    if not nextPageKey:
        break

# Create a set to store all entities with at least one applied SLO
entities_with_slos = set()

# Retrieve entities for each SLO's filter
for slo in slos:
    entity_filter = slo['filter']
    # Construct API request URL for entities that match the filter
    url = f'https://{ENVIRONEMNT_ID}.live.dynatrace.com/api/v2/entities?entitySelector={entity_filter}&fields=+tags'
    response = requests.get(url, headers=headers)
    data = json.loads(response.text)
    entities = data['entities']
    # Add entities to the set if they are not already in there
    for entity in entities:
        entity_identifier = entity['displayName']
        tags = entity.get('tags', [])  # Get the tags list, or an empty list if it's not present
        for tag in tags:
            if tag.get('key') == 'proj':
                entity_identifier = tag.get('value')
        entities_with_slos.add(entity_identifier)

# Convert the set of entities to a list and print it
entities_with_slos_list = list(entities_with_slos)

print(entities_with_slos_list)

for service in entities_with_slos_list:
    entity = {
        "identifier": service,
        "properties": {
            "slo": True
        }
    }
    add_entity_to_port(entity)