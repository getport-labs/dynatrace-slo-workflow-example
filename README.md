# Port Workflow with Dynatrace


## Getting started

In this example you will create a blueprint for `service` entity that checks and ingests if a service has any `slo` defined in dynatrace.


### Gitlab CI yaml
Place this example `.gitlab-ci.yml` file in your project's root folder

### Gitlab CI Variables
To interact with Port using Gitlab CI Pipeline, you will first need to define your Port credentials [as variables for your pipeline](https://docs.gitlab.com/ee/ci/variables/index.html#define-a-cicd-variable-in-the-ui). Then, pass the defined variables to your ci pipeline script. Ensure to add your Dynatrace API key and Evnironment ID(the prefix of the dyna url, <envid>.live.dyntrace) as well to the variables.

The list of the required variables to run this pipeline are
- `PORT_CLIENT_ID`
- `PORT_CLIENT_SECRET`
- `DYNATRACE_API_KEY`
- `DYNATRACE_ENVIRONMENT_ID`

### Schedule the script
1. Go to your Gitlab project and select CI/CD
2. Click on **Schedules** and create new schedule
3. Enter the necessary information into the form: the Description, Interval Pattern, Timezone, Target branch and other variables specifically for the schedule.
4. Click on **Save pipeline schedule** 

#### Screenshot - Schedule
![screenshot image](./assets/schedule.PNG "Oncall Schedule Trigger in Gitlab")

#### Screenshot - Pipeline Success
![screenshot image](./assets/pipeline.PNG "Successful Gitlab Pipeline Scheduled")
